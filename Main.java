/**
 * @import import is basically used to import the different packages of java in our program. 
 */
import java.util.ArrayList;
import java.io.*;
import java.util.Scanner;
/**
 * @Electricity_Bill_Gen This is our public class which contains main method.
 */
public class Electricity_Bill_Gen {
	 static int EBConn;
/**
 * @param args is a reference variable of String Array. 
 * @throws Exception used to hand over the Exception object to JVM. 
 */
	public static void main(String[] args)throws Exception {
		Scanner sc= new Scanner(System.in);

		System.out.println("Welcome Electricity Bill Genetation");
	
		String option = "y"; 
		while(option.equals("y") || option.equals("yes")) {
			
		System.out.println("Connection Type: 1.Domestic 2.Commercial (1 or 2):");
	
			try {
				EBConn = sc.nextInt();
				if(!(EBConn == 1 || EBConn == 2)) {
					throw new InputDataException("Invalid connection type");
				}
			}catch(InputDataException ide) {
				System.out.println(ide);
			}catch(Exception e) {
				System.out.print(e);
				System.out.println(" : Invalid connection Type");
			}
/**
 * @switch is used to choose the type of EBConn(Domestic,Commercial).
 * A @FileWriter object is passed to the @BufferedWriter as the intent here is to write to some output file using a BufferedWriter.
 *  And finally, a @PrintWriter is used for print* methods like @println()			
 */
			
			switch(EBConn) {
			case 1:Domestic DM= new Domestic();
			DM.input_data();
			DM.display();
			DM.setEBConn(EBConn);

			BufferedWriter BW1 = new BufferedWriter(new FileWriter("Domestic.txt",true));
			PrintWriter pw1 = new PrintWriter(BW1);
			String custDetails = DM.getCustomerDetails();
			pw1.println(custDetails);
			
			pw1.close();
			System.out.println(custDetails);
			break;
			
			case 2:Commercial CM = new Commercial();
			CM.input_data() ;
			CM.display();
			CM.setEBConn(EBConn);
			BufferedWriter BW2 = new BufferedWriter(new FileWriter("Commercial.txt",true));
			PrintWriter pw2 = new PrintWriter(BW2);
			
			custDetails = CM.getCustomerDetails();
			pw2.println(custDetails);
			pw2.close();
			System.out.println(custDetails);
		
			}
			System.out.println("Enter 'y' or 'yes' to continue");
			option = sc.next().toLowerCase();
		}
			
	}
}
